package in.aether.bundles.flink.jobs;/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import in.aether.bundles.flink.models.IssueBundleRequest;
import in.aether.bundles.flink.processors.BundleIssuanceEvent;
import in.aether.bundles.flink.processors.IssueAccountProducts;
import in.aether.bundles.flink.processors.IssuePaymentProducts;
import in.aether.bundles.flink.processors.ValidatorService;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.configuration.TaskManagerOptions;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.util.serialization.SimpleStringSchema;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * Skeleton code for the datastream walkthrough
 */
public class IssueBundleJob {
	public static void main(String[] args) throws Exception {
		StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

		final Configuration configuration = new Configuration();
		configuration.setInteger(TaskManagerOptions.NUM_TASK_SLOTS,6);

		String inputTopic = "issue_bundle7";
		String consumerGroup = "consumerGroup";
		String address = "localhost:9092";
		Properties properties = new Properties();
		properties.setProperty("bootstrap.servers", "localhost:9092");
		properties.setProperty("group.id", "test");
		IssueBundleRequest issueBundleRequest = new IssueBundleRequest();
		ObjectMapper mapper = new ObjectMapper();
		String obj = mapper.writeValueAsString(issueBundleRequest);
		DataStreamSource<String> stream =  env
				.fromElements(obj,obj);

		DataStream<String> stream1 = env
				.addSource(new FlinkKafkaConsumer<>("issue_bundle7", new SimpleStringSchema(), properties))
				.name("Kafka Stream")
				.rebalance();

		List<String> services = Arrays.asList("service1","Serivece2");
		DataStream<String> prev = stream1;
		for(String service: services){
			DataStream<IssueBundleRequest> filtered = stream1
					.process(new ValidatorService())
					.name("ValidationService").rebalance().filter(new FilterFunction<IssueBundleRequest>() {
						@Override
						public boolean filter(IssueBundleRequest issueBundleRequest) throws Exception {
							return issueBundleRequest.getEnrichmentDetails()!=null;
						}
					});
			prev = stream1;
		}


		DataStream<IssueBundleRequest> filtered = stream1
			.process(new ValidatorService())
				.name("ValidationService").rebalance();

		DataStream<IssueBundleRequest> accountProducts = filtered
				.process(new IssueAccountProducts())
				.setParallelism(1)
				.name("IssueAccountProducts").rebalance();

		DataStream<IssueBundleRequest> paymentProducts = accountProducts
				.process(new IssuePaymentProducts())
				.setParallelism(1)
				.name("IssuePaymentProducts")
				.rebalance();

		DataStream<IssueBundleRequest> bundle_issuance_event = paymentProducts.process(new BundleIssuanceEvent())
				.name("Bundle Issuance Event")
				.setParallelism(1).rebalance();


		env.execute("Issue Bundles");
	}
}
