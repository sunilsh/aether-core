package in.aether.bundles.flink.jobs;

import in.aether.bundles.flink.models.BatchTaskExecutorDetails;
import in.aether.bundles.flink.models.IssueBundleRequest;
import in.aether.bundles.flink.models.TaskExecutorDetails;
import in.aether.bundles.flink.processors.*;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.configuration.TaskManagerOptions;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.IterativeStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import org.apache.flink.streaming.util.serialization.SimpleStringSchema;

import java.util.Properties;

public class IssueBundleWithBatchPlanner {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        final Configuration configuration = new Configuration();
        configuration.setInteger(TaskManagerOptions.NUM_TASK_SLOTS,6);
        ObjectMapper objectMapper = new ObjectMapper();
        String inputTopic = "issue_bundle7";
        String consumerGroup = "BundlesConsumerGroup";
        String address = "localhost:9092";
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", address);
        properties.setProperty("group.id", consumerGroup);
        DataStream<String> stream1 = env
                .addSource(new FlinkKafkaConsumer<>(inputTopic, new SimpleStringSchema(), properties))
                .name("Kafka Stream")
                .rebalance();

        IterativeStream<IssueBundleRequest> planner = stream1
                .process(new BatchPlanner())
                .name("Planner").iterate();


        DataStream<BatchTaskExecutorDetails> taskProcessor = planner
                .process(new ProcessTasks())
                .name("taskProcessor");


        DataStream<TaskExecutorDetails> batchTasks = taskProcessor
                .process(new ExecuteBatchTasks())
                .name("batchTasks");

        DataStream<TaskExecutorDetails> executedTasks = batchTasks
                .process(new ExecuteTasksProcess())
                .setParallelism(2)
                .name("executedTasks")
                .keyBy(new KeySelector<TaskExecutorDetails, Object>() {
                    @Override
                    public Object getKey(TaskExecutorDetails taskExecutorDetails) throws Exception {
                        return taskExecutorDetails.getIssueBundleRequest().getId();
                    }
                });

        FlinkKafkaProducer<String> myProducer = new FlinkKafkaProducer<String>(
                "my-topic",
                new org.apache.flink.api.common.serialization.SimpleStringSchema(),
                properties);


        executedTasks.map(new MapFunction<TaskExecutorDetails, String>() {
            @Override
            public String map(TaskExecutorDetails taskExecutorDetails) throws Exception {
                return objectMapper.writeValueAsString(taskExecutorDetails);
            }
        }).addSink(myProducer).name("Push Events");

        DataStream<IssueBundleRequest> taskCollector = executedTasks
                .process(new TaskCollector())
                .setParallelism(2)
                .name("collectorTasks")
                .keyBy(new BundleKeySelector());

        DataStream<IssueBundleRequest> terminatingFilter = taskCollector.filter(new FilterFunction<IssueBundleRequest>() {
            @Override
            public boolean filter(IssueBundleRequest issueBundleRequest) throws Exception {
                return issueBundleRequest.getCurrentBatchTask()<issueBundleRequest.getTasks().size();
            }
        });

        planner.closeWith(terminatingFilter);
        SingleOutputStreamOperator<String> persistentEvent = taskCollector
                .filter(new FilterFunction<IssueBundleRequest>() {
                    @Override
                    public boolean filter(IssueBundleRequest issueBundleRequest) throws Exception {
                        return issueBundleRequest.getCurrentBatchTask()==issueBundleRequest.getTasks().size();
                    }
                })
                .process(new PersistenceEvent())
                .name("persistentEvent");

        persistentEvent.addSink(myProducer).name("KafkaStream");
        env.execute("Issue Bundle Scheduler");

    }
}
