package in.aether.bundles.flink.models;

import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.JsonNode;

import java.io.Serializable;
import java.util.*;

@JsonIgnoreProperties
public class IssueBundleRequest implements Serializable {
    String id;
    Long eventTime;
    Long timeStamp;
    String enrichmentDetails;
    String status = "initiated";
    List<List<TaskDetails>> tasks = new LinkedList<>();
    Integer currentBatchTask = 0;
    Integer totalTasks = 0;

    public Integer getCurrentBatchTask() {
        return currentBatchTask;
    }

    public void setCurrentBatchTask(Integer currentBatchTask) {
        this.currentBatchTask = currentBatchTask;
    }

    public List<List<TaskDetails>> getTasks() {
        return tasks;
    }

    public void setTasks(List<List<TaskDetails>> tasks) {
        this.tasks = tasks;
    }
    public Integer getTotalTasks() {
        return totalTasks;
    }

    public void setTotalTasks(Integer totalTasks) {
        this.totalTasks = totalTasks;
    }

    HashMap<Integer,TaskDetails> dependentTasksDetails = new HashMap<>();
    HashMap<Integer,TaskDetails> allTasksDetails = new HashMap<>();
    HashMap<Integer,TaskDetails> independentTasksDetails = new HashMap<>();
    List<Integer> completedTasks = new ArrayList<>();
    Map<Integer,TaskDetails> completedTaskDetails = new HashMap<>();
    HashMap<Integer, List<Integer>> graph = new HashMap<>();
    List<Integer> totalDependentTasks = new ArrayList<>();
    Integer totalTasksForCurrentExecutors;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getTotalTasksForCurrentExecutors() {
        return totalTasksForCurrentExecutors;
    }

    public void setTotalTasksForCurrentExecutors(Integer totalTasksForCurrentExecutors) {
        this.totalTasksForCurrentExecutors = totalTasksForCurrentExecutors;
    }

    public HashMap<Integer, TaskDetails> getAllTasksDetails() {
        return allTasksDetails;
    }

    public void setAllTasksDetails(HashMap<Integer, TaskDetails> allTasksDetails) {
        this.allTasksDetails = allTasksDetails;
    }

    public Map<Integer, TaskDetails> getCompletedTaskDetails() {
        return completedTaskDetails;
    }

    public void setCompletedTaskDetails(Map<Integer, TaskDetails> completedTaskDetails) {
        this.completedTaskDetails = completedTaskDetails;
    }

    public HashMap<Integer, TaskDetails> getIndependentTasksDetails() {
        return independentTasksDetails;
    }

    public void setIndependentTasksDetails(HashMap<Integer, TaskDetails> independentTasksDetails) {
        this.independentTasksDetails = independentTasksDetails;
    }

    public HashMap<Integer, TaskDetails> getDependentTasksDetails() {
        return dependentTasksDetails;
    }
    public void setDependentTasksDetails(HashMap<Integer, TaskDetails> dependentTasksDetails) {
        this.dependentTasksDetails = dependentTasksDetails;
    }

    public void setTotalDependentTasks(List<Integer> totalDependentTasks) {
        this.totalDependentTasks = totalDependentTasks;
    }


    public void setId(String id) {
        this.id = id;
    }

    public void setEventTime(Long eventTime) {
        this.eventTime = eventTime;
    }

    public void setEnrichmentDetails(String enrichmentDetails) {
        this.enrichmentDetails = enrichmentDetails;
    }

    public HashMap<Integer, List<Integer>> getGraph() {
        return graph;
    }

    public List<Integer> getTotalDependentTasks() {
        return totalDependentTasks;
    }

    public List<Integer> getCompletedTasks() {
        return completedTasks;
    }

    public void setCompletedTasks(List<Integer> completedTasks) {
        this.completedTasks = completedTasks;
    }

    public void setGraph(HashMap<Integer, List<Integer>> graph) {
        this.graph = graph;
    }

    public String getEnrichmentDetails() {
        return enrichmentDetails;
    }

    public Long getEventTime() {
        return eventTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id,Long eventTime) {
        this.id = id;
        this.eventTime = eventTime;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public IssueBundleRequest(String id, Long timeStamp) {
        this.id = id;
        this.timeStamp = timeStamp;
    }
    public IssueBundleRequest() {
    }

    @Override
    public String toString() {
        return "IssueBundleRequest{" +
                "id='" + id + '\'' +
                ", eventTime=" + eventTime +
                ", timeStamp=" + timeStamp +
                ", allTasks='" + tasks + '\'' +
                ", enrichmentDetails='" + enrichmentDetails + '\'' +
                ", status='" + status + '\'' +
                ", dependentTasksDetails=" + dependentTasksDetails +
                ", allTasksDetails=" + allTasksDetails +
                ", independentTasksDetails=" + independentTasksDetails +
                ", completedTasks=" + completedTasks +
                ", completedTaskDetails=" + completedTaskDetails +
                ", graph=" + graph +
                ", totalDependentTasks=" + totalDependentTasks +
                ", totalTasksForCurrentExecutors=" + totalTasksForCurrentExecutors +
                '}';
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }
}
