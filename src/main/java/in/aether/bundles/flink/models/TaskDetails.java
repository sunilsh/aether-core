package in.aether.bundles.flink.models;

import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.JsonNode;

import java.io.Serializable;

public class TaskDetails implements Serializable {
    Integer id;
    String status;
    String name;
    JsonNode taskData;
    Integer executingBatch;
    Long endTime;

    public TaskDetails() {
    }

    public TaskDetails(Integer id, String status, String name) {
        this.id = id;
        this.status = status;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public JsonNode getTaskData() {
        return taskData;
    }

    public Integer getExecutingBatch() {
        return executingBatch;
    }

    public void setExecutingBatch(Integer executingBatch) {
        this.executingBatch = executingBatch;
    }

    @Override
    public String toString() {
        return "TaskDetails{" +
                "id=" + id +
                ", status='" + status + '\'' +
                ", name='" + name + '\'' +
                ", taskData=" + taskData +
                ", executingBatch=" + executingBatch +
                ", endTime=" + endTime +
                '}';
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public void setTaskData(JsonNode taskData) {
        this.taskData = taskData;
    }
}
