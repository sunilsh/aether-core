package in.aether.bundles.flink.models;

import java.util.List;

public class BatchTaskExecutorDetails {
    IssueBundleRequest issueBundleRequest;
    List<TaskDetails> batchTaskDetails;
    Integer executingBatch;

    public BatchTaskExecutorDetails(IssueBundleRequest issueBundleRequest, List<TaskDetails> batchTaskDetails,Integer executingBatch) {
        this.issueBundleRequest = issueBundleRequest;
        this.batchTaskDetails = batchTaskDetails;
        this.executingBatch = executingBatch;
    }

    public IssueBundleRequest getIssueBundleRequest() {
        return issueBundleRequest;
    }

    public void setIssueBundleRequest(IssueBundleRequest issueBundleRequest) {
        this.issueBundleRequest = issueBundleRequest;
    }

    public List<TaskDetails> getBatchTaskDetails() {
        return batchTaskDetails;
    }

    public void setBatchTaskDetails(List<TaskDetails> batchTaskDetails) {
        this.batchTaskDetails = batchTaskDetails;
    }

    public Integer getExecutingBatch() {
        return executingBatch;
    }
}
