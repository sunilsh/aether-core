package in.aether.bundles.flink.models;

import java.util.HashMap;

public class HttpRequestModel {
    String body;
    HashMap<String,String> headers;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public HashMap<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(HashMap<String, String> headers) {
        this.headers = headers;
    }
}
