package in.aether.bundles.flink.models;

public class TaskExecutorDetails {
    IssueBundleRequest issueBundleRequest;
    TaskDetails taskDetails;

    public TaskExecutorDetails(IssueBundleRequest issueBundleRequest, TaskDetails taskDetails) {
        this.issueBundleRequest = issueBundleRequest;
        this.taskDetails = taskDetails;
    }

    public IssueBundleRequest getIssueBundleRequest() {
        return issueBundleRequest;
    }

    public void setIssueBundleRequest(IssueBundleRequest issueBundleRequest) {
        this.issueBundleRequest = issueBundleRequest;
    }

    public TaskDetails getTaskDetails() {
        return taskDetails;
    }

    public void setTaskDetails(TaskDetails taskDetails) {
        this.taskDetails = taskDetails;
    }
}
