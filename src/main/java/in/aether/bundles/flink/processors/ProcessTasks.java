package in.aether.bundles.flink.processors;

import in.aether.bundles.flink.models.BatchTaskExecutorDetails;
import in.aether.bundles.flink.models.IssueBundleRequest;
import in.aether.bundles.flink.models.TaskDetails;
import in.aether.bundles.flink.models.TaskExecutorDetails;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;


public class ProcessTasks extends ProcessFunction<IssueBundleRequest, BatchTaskExecutorDetails> {

    Logger logger = LoggerFactory.getLogger(ProcessTasks.class);
    @Override
    public void processElement(IssueBundleRequest issueBundleRequest, Context context, Collector<BatchTaskExecutorDetails> collector) throws Exception {

        //logger.info("PreCompletedTasksDetails {}",completedTasksDetails.value());
        Integer currentBatchTask = issueBundleRequest.getCurrentBatchTask();
        issueBundleRequest.setTotalTasksForCurrentExecutors(issueBundleRequest.getTasks().get(currentBatchTask).size());
        List<TaskDetails> tasks = issueBundleRequest.getTasks().get(currentBatchTask);
        BatchTaskExecutorDetails batchTaskExecutorDetails = new BatchTaskExecutorDetails(issueBundleRequest,tasks,issueBundleRequest.getCurrentBatchTask());
        logger.info("going to execute current batch {}", currentBatchTask);
        collector.collect(batchTaskExecutorDetails);
    }
}
