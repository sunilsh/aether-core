package in.aether.bundles.flink.processors;

import in.aether.bundles.flink.models.BatchTaskExecutorDetails;
import in.aether.bundles.flink.models.IssueBundleRequest;
import in.aether.bundles.flink.models.TaskDetails;
import in.aether.bundles.flink.models.TaskExecutorDetails;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ExecuteBatchTasks extends ProcessFunction<BatchTaskExecutorDetails,TaskExecutorDetails> {

    Logger logger = LoggerFactory.getLogger(ExecuteBatchTasks.class);

    private transient ValueState<List<Integer>> completedTasks;
    private transient ValueState<Map<Integer, TaskDetails>> completedTasksDetails;

    @Override
    public void processElement(BatchTaskExecutorDetails batchTaskExecutorDetails, Context context, Collector<TaskExecutorDetails> collector) throws Exception {
        IssueBundleRequest issueBundleRequest = batchTaskExecutorDetails.getIssueBundleRequest();
        //issueBundleRequest.setTotalTasksForCurrentExecutors(issueBundleRequest.getIndependentTasksDetails().size());
        for(TaskDetails task: batchTaskExecutorDetails.getBatchTaskDetails()){
            TaskExecutorDetails taskExecutorDetails = new TaskExecutorDetails(issueBundleRequest,task);
            taskExecutorDetails.getTaskDetails().setExecutingBatch(batchTaskExecutorDetails.getExecutingBatch());
            collector.collect(taskExecutorDetails);
        }
    }
}
