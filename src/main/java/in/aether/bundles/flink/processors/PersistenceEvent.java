package in.aether.bundles.flink.processors;

import in.aether.bundles.flink.models.IssueBundleRequest;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.util.Collector;

public class PersistenceEvent extends ProcessFunction<IssueBundleRequest,String> {

    ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void processElement(IssueBundleRequest issueBundleRequest, Context context, Collector<String> collector) throws Exception {
        issueBundleRequest.setStatus("issued");
        persistEvent(issueBundleRequest);
        collector.collect(objectMapper.writeValueAsString(issueBundleRequest));
    }
    //persist
    private  void persistEvent(IssueBundleRequest issueBundleRequest){

    }
}
