package in.aether.bundles.flink.processors;

import in.aether.bundles.flink.models.IssueBundleRequest;
import in.aether.bundles.flink.models.TaskDetails;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class ValidatorService extends ProcessFunction<String, IssueBundleRequest> {

    Logger logger = LoggerFactory.getLogger(ValidatorService.class);
    ObjectMapper mapper = new ObjectMapper();

    @Override
    public void processElement(String issueBundleRequest, Context context, Collector<IssueBundleRequest> collector) throws Exception {
        logger.info("executing issueBundleRequest {}", issueBundleRequest);
        IssueBundleRequest issueBundleRequest1 = mapper.convertValue(mapper.readTree(issueBundleRequest), IssueBundleRequest.class);
        issueBundleRequest1.setTimeStamp(System.currentTimeMillis());
        HashMap<Integer, TaskDetails> independentTaskDetails = new HashMap<>();
        TaskDetails getAccountProduct = new TaskDetails(1,"pending","GetAccountProduct");
        TaskDetails getPaymentProduct = new TaskDetails(2,"pending","getPaymentProduct");
        TaskDetails getResource = new TaskDetails(3,"pending","getResourceProduct");
        independentTaskDetails.put(getAccountProduct.getId(),getAccountProduct);
        independentTaskDetails.put(getPaymentProduct.getId(),getPaymentProduct);
        independentTaskDetails.put(getResource.getId(),getResource);
        issueBundleRequest1.setIndependentTasksDetails(independentTaskDetails);
        HashMap<Integer, TaskDetails> dependentTaskDetails = new HashMap<>();
        TaskDetails createResource = new TaskDetails(4,"pending","createResource");
        TaskDetails createAccount = new TaskDetails(5,"pending","createAccount");
        dependentTaskDetails.put(4,createResource);
        dependentTaskDetails.put(5,createAccount);
        HashMap<Integer, List<Integer>> graph1 = new HashMap<>();
        graph1.put(4, Arrays.asList(5));
        issueBundleRequest1.setGraph(graph1);
        issueBundleRequest1.setDependentTasksDetails(dependentTaskDetails);
        issueBundleRequest1.setTotalDependentTasks(Arrays.asList(4,5));
        HashMap<Integer, TaskDetails> allTasks = new HashMap<>();
        allTasks.putAll(dependentTaskDetails);
        allTasks.putAll(independentTaskDetails);
        issueBundleRequest1.setAllTasksDetails(allTasks);
        //logger.info("issueBundleRequest1 received {}", issueBundleRequest1);
        collector.collect(issueBundleRequest1);
    }
}
