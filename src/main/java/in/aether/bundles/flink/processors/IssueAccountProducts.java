package in.aether.bundles.flink.processors;

import in.aether.bundles.flink.models.IssueBundleRequest;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IssueAccountProducts extends ProcessFunction<IssueBundleRequest,IssueBundleRequest> implements SinkFunction<IssueBundleRequest> {

    Logger logger = LoggerFactory.getLogger(IssueAccountProducts.class);

    @Override
    public void processElement(IssueBundleRequest issueBundleRequest, ProcessFunction<IssueBundleRequest, IssueBundleRequest>.Context context, Collector<IssueBundleRequest> collector) throws Exception {
        //logger.info("IssueAccountProducts received for issueBundleRequest1  {}", issueBundleRequest);
        collector.collect(issueBundleRequest);
    }

    @Override
    public void invoke(IssueBundleRequest issueBundleRequest) throws Exception {

    }
}
