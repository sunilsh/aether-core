package in.aether.bundles.flink.processors;

import in.aether.bundles.flink.models.IssueBundleRequest;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.metrics.Histogram;
import org.apache.flink.metrics.HistogramStatistics;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BundleIssuanceEvent extends ProcessFunction<IssueBundleRequest,IssueBundleRequest> {

    Logger logger = LoggerFactory.getLogger(BundleIssuanceEvent.class);
    private transient Histogram histogram;


    @Override
    public void open(Configuration config) {
        this.histogram = getRuntimeContext()
                .getMetricGroup()
                .histogram("myHistogram", new Histogram() {
                    @Override
                    public void update(long l) {

                    }

                    @Override
                    public long getCount() {
                        return 0;
                    }

                    @Override
                    public HistogramStatistics getStatistics() {
                        return null;
                    }
                });
    }

    @Override
    public void processElement(IssueBundleRequest issueBundleRequest, Context context, Collector<IssueBundleRequest> collector) throws Exception {
        logger.info("Bundle issuance Successful sending event");
        histogram.update(500);
        collector.collect(issueBundleRequest);
    }
}
