package in.aether.bundles.flink.processors;

import in.aether.bundles.flink.models.IssueBundleRequest;
import in.aether.bundles.flink.models.TaskDetails;
import in.aether.bundles.flink.models.TaskExecutorDetails;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.JsonNode;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.*;

public class ExecuteTasksProcess extends ProcessFunction<TaskExecutorDetails, TaskExecutorDetails> {

    Logger logger = LoggerFactory.getLogger(ExecuteTasksProcess.class);
    ObjectMapper mapper = new ObjectMapper();

    @Override
    public void processElement(TaskExecutorDetails taskExecutorDetails, Context context, Collector<TaskExecutorDetails> collector) throws Exception {
        IssueBundleRequest issueBundleRequest = taskExecutorDetails.getIssueBundleRequest();
        logger.info("getIssueBundleRequest tasks{}",taskExecutorDetails.getIssueBundleRequest());
        logger.info("executing task details {}", taskExecutorDetails.getTaskDetails());
        //issueBundleRequest.getCompletedTasks().add(issueBundleRequest.getCurrentTask());
        TaskDetails taskDetails = getUpdatedTaskDetails(issueBundleRequest,taskExecutorDetails.getTaskDetails().getId(),taskExecutorDetails.getTaskDetails());
        collector.collect(new TaskExecutorDetails(issueBundleRequest,taskDetails));
        logger.info("executing tasks completed {}", taskDetails.getId());
    }
    private TaskDetails getUpdatedTaskDetails(IssueBundleRequest issueBundleRequest,Integer currentTask,TaskDetails currentTaskDetails) throws InterruptedException {
        logger.info("executing some expensive steps for current tasks {}",currentTask);
        Thread.sleep(100);
        HashMap<String,String> map = new HashMap<>();
        map.put(currentTaskDetails.getName(),"DataFetched");
        map.put("metadata","metadata");
        currentTaskDetails.setStatus("finished");
        currentTaskDetails.setTaskData(mapper.convertValue(map, JsonNode.class));
        logger.info("current Task details {}",currentTaskDetails);

        currentTaskDetails.setEndTime(System.currentTimeMillis());
        return currentTaskDetails;
    }

}
