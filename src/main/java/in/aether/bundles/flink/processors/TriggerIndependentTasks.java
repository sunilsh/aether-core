package in.aether.bundles.flink.processors;

import in.aether.bundles.flink.models.IssueBundleRequest;
import in.aether.bundles.flink.models.TaskDetails;
import in.aether.bundles.flink.models.TaskExecutorDetails;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class TriggerIndependentTasks extends ProcessFunction<IssueBundleRequest,TaskExecutorDetails> {

    Logger logger = LoggerFactory.getLogger(TriggerIndependentTasks.class);

    private transient ValueState<List<Integer>> completedTasks;
    private transient ValueState<Map<Integer, TaskDetails>> completedTasksDetails;

    @Override
    public void open(Configuration conf) {
        // register state handle
        completedTasks = getRuntimeContext().getState(
                new ValueStateDescriptor<>("completed", Types.LIST(Types.INT)));
        completedTasksDetails = getRuntimeContext().getState(
                new ValueStateDescriptor<>("completedTaskData", Types.MAP(Types.INT,Types.POJO(TaskDetails.class))));
    }
    @Override
    public void processElement(IssueBundleRequest issueBundleRequest, Context context, Collector<TaskExecutorDetails> collector) throws Exception {
        issueBundleRequest.setTotalTasksForCurrentExecutors(issueBundleRequest.getIndependentTasksDetails().size());
        if(completedTasks.value() == null){
            completedTasks.update(new ArrayList<>());
        }
        if(completedTasksDetails.value()==null){
            completedTasksDetails.update(new HashMap<>());
        }
        logger.info("PreCompletedTasksDetails {}",completedTasksDetails.value());
        for(Integer tasks: issueBundleRequest.getIndependentTasksDetails().keySet()){
            TaskDetails taskDetails = issueBundleRequest.getIndependentTasksDetails().get(tasks);
            TaskExecutorDetails taskExecutorDetails = new TaskExecutorDetails(issueBundleRequest,taskDetails);
            collector.collect(taskExecutorDetails);
        }
    }
}
