package in.aether.bundles.flink.processors;

import in.aether.bundles.flink.filters.TaskFilter;
import in.aether.bundles.flink.models.IssueBundleRequest;
import in.aether.bundles.flink.models.TaskExecutorDetails;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.configuration.TaskManagerOptions;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;

import java.util.*;

public class SolveGraphJob {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        final Configuration configuration = new Configuration();
        configuration.setInteger(TaskManagerOptions.NUM_TASK_SLOTS,6);
        HashMap<String, List<String>> graph2 = new HashMap<>();
        IssueBundleRequest issueBundleRequest = new IssueBundleRequest();
        String inputTopic = "issue_bundle7";
        String consumerGroup = "consumerGroup";
        String address = "localhost:9092";
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", "localhost:9092");
        properties.setProperty("group.id", "test1234");
        ObjectMapper mapper = new ObjectMapper();
        DataStream<String> stream1 = env
                .addSource(new FlinkKafkaConsumer<>("issue_bundle7", new SimpleStringSchema(), properties))
                .name("Kafka Stream");

        DataStream<IssueBundleRequest> filtered = stream1
                .process(new ValidatorService())
                .name("ValidationService")
                .keyBy(new BundleKeySelector());

        DataStream<TaskExecutorDetails> managedTask = filtered
                .process(new TriggerIndependentTasks())
                .name("managedTask");

        DataStream<TaskExecutorDetails> executedTasks = managedTask
                .process(new ExecuteTasksProcess())
                .setParallelism(2)
                .name("executedTasks")
                .keyBy(new KeySelector<TaskExecutorDetails, Object>() {
                    @Override
                    public Object getKey(TaskExecutorDetails taskExecutorDetails) throws Exception {
                        return taskExecutorDetails.getIssueBundleRequest().getId();
                    }
                });
        DataStream<IssueBundleRequest> taskCollector = executedTasks
                .process(new TaskCollector())
                .setParallelism(2)
                .name("collectorTasks")
                .keyBy(new BundleKeySelector())
                .filter(new TaskFilter());

        DataStream<TaskExecutorDetails> dependentTaskExecutor = taskCollector
                .process(new TriggerDependentTasks())
                .name("dependentTasks")
                .keyBy(new KeySelector<TaskExecutorDetails, Object>() {
                    @Override
                    public Object getKey(TaskExecutorDetails taskExecutorDetails) throws Exception {
                        return taskExecutorDetails.getIssueBundleRequest().getId();
                    }
                });
        DataStream<TaskExecutorDetails> executedDependentTasks = dependentTaskExecutor
                .process(new ExecuteTasksProcess())
                .setParallelism(2)
                .name("executeDependentTasks").keyBy(new KeySelector<TaskExecutorDetails, Object>() {
                    @Override
                    public Object getKey(TaskExecutorDetails taskExecutorDetails) throws Exception {
                        return taskExecutorDetails.getIssueBundleRequest().getId();
                    }
                });
        DataStream<IssueBundleRequest> dependentTaskCollector = executedDependentTasks
                .process(new TaskCollector())
                .setParallelism(1)
                .name("dependentTaskCollector");

        SingleOutputStreamOperator<String> persistentEvent = dependentTaskCollector
                .process(new PersistenceEvent())
                .name("persistentEvent");


        FlinkKafkaProducer<String> myProducer = new FlinkKafkaProducer<String>(
                "my-topic",
                new SimpleStringSchema(),
                properties);
        persistentEvent.addSink(myProducer).name("KafkaStream");
        env.execute("Complete tasks");
    }
}
