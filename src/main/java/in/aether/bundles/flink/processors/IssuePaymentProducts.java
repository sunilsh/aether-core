package in.aether.bundles.flink.processors;

import in.aether.bundles.flink.models.IssueBundleRequest;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IssuePaymentProducts extends ProcessFunction<IssueBundleRequest,IssueBundleRequest> implements SinkFunction<IssueBundleRequest> {

    Logger logger = LoggerFactory.getLogger(IssueAccountProducts.class);

    @Override
    public void processElement(IssueBundleRequest issueBundleRequest, ProcessFunction<IssueBundleRequest, IssueBundleRequest>.Context context, Collector<IssueBundleRequest> collector) throws Exception {
        //logger.info("IssuePaymentProducts received for issueBundleRequest  {}", issueBundleRequest);
        logger.info("time taken to issue bundles  with request {}: {}",issueBundleRequest, System.currentTimeMillis()-issueBundleRequest.getTimeStamp());
        logger.info("delay time "+ (issueBundleRequest.getTimeStamp()-issueBundleRequest.getEventTime()*1000));
        collector.collect(issueBundleRequest);
    }

    @Override
    public void invoke(IssueBundleRequest issueBundleRequest) throws Exception {

    }
}
