package in.aether.bundles.flink.processors;

import in.aether.bundles.flink.models.IssueBundleRequest;
import in.aether.bundles.flink.models.TaskDetails;
import in.aether.bundles.flink.models.TaskExecutorDetails;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class TaskCollector extends ProcessFunction<TaskExecutorDetails,IssueBundleRequest> {

    private transient ValueState<List<Integer>> completedTasks;
    private transient ValueState<Map<Integer, TaskDetails>> completedTasksDetails;
    private transient ValueState<List<TaskDetails>> dynamicTasksForCurrentBatch;


    Logger logger = LoggerFactory.getLogger(TaskCollector.class);

    @Override
    public void open(Configuration conf) {
        // register state handle
        completedTasks = getRuntimeContext().getState(
                new ValueStateDescriptor<>("completed", Types.LIST(Types.INT)));
        dynamicTasksForCurrentBatch = getRuntimeContext().getState(
                new ValueStateDescriptor<>("dynamicTasksForCurrentBatch", Types.LIST(Types.POJO(TaskDetails.class))));
        completedTasksDetails = getRuntimeContext().getState(
                new ValueStateDescriptor<>("completedTaskData", Types.MAP(Types.INT,Types.POJO(TaskDetails.class))));
    }

    @Override
    public void processElement(TaskExecutorDetails taskExecutorDetails, Context context, Collector<IssueBundleRequest> collector) throws Exception {
        IssueBundleRequest issueBundleRequest = taskExecutorDetails.getIssueBundleRequest();
        TaskDetails taskDetails = taskExecutorDetails.getTaskDetails();
        if(completedTasks.value() == null){
            completedTasks.update(new ArrayList<>());
        }
        if(completedTasksDetails.value()==null){
            completedTasksDetails.update(new HashMap<>());
        }
        if(taskDetails.getName().equalsIgnoreCase("GetAccountProduct")) {
            logger.info("Hey I found a new task ");
            TaskDetails newTaskDetails = new TaskDetails();
            newTaskDetails.setStatus("pending");
            newTaskDetails.setName("CreateWalletProduct");
            if(dynamicTasksForCurrentBatch.value()==null){
                dynamicTasksForCurrentBatch.update(new ArrayList<TaskDetails>());
            }
            Integer currentSize = dynamicTasksForCurrentBatch.value().size();
            newTaskDetails.setId(issueBundleRequest.getTotalTasks()+currentSize+1);
            dynamicTasksForCurrentBatch.value().add(newTaskDetails);
        }
        Integer currentTask = taskDetails.getId();
        completedTasks.value().add(currentTask);
        completedTasksDetails.value().put(currentTask,taskDetails);
        logger.info("completed tasks count : {}, value {}",completedTasks.value().size(),completedTasks.value());
        logger.info("saving completed tasks {} : details {}",taskDetails.getId(),taskExecutorDetails.getTaskDetails());
        if(completedTasks.value().size()==issueBundleRequest.getTotalTasksForCurrentExecutors()){
            logger.info("all tasks completed {} {}",completedTasks.value(),completedTasksDetails.value());
            for(Integer task: completedTasksDetails.value().keySet()){
                issueBundleRequest.getAllTasksDetails().put(task,completedTasksDetails.value().get(task));
                issueBundleRequest.getCompletedTasks().add(task);
                if(issueBundleRequest.getIndependentTasksDetails().containsKey(task)){
                    issueBundleRequest.getIndependentTasksDetails().put(task,completedTasksDetails.value().get(task));
                }
                if(issueBundleRequest.getDependentTasksDetails().containsKey(task)){
                    issueBundleRequest.getDependentTasksDetails().put(task,completedTasksDetails.value().get(task));
                }
            }
            if(dynamicTasksForCurrentBatch.value()!=null && dynamicTasksForCurrentBatch.value().size()>0){
                issueBundleRequest.setTotalTasks(issueBundleRequest.getTotalTasks()+dynamicTasksForCurrentBatch.value().size());
                issueBundleRequest.getTasks().add(dynamicTasksForCurrentBatch.value());
            }
            logger.info("current batch request with id: {} is completed and data populated so far{}",issueBundleRequest.getCurrentBatchTask(),issueBundleRequest.getCompletedTasks());
            issueBundleRequest.setCurrentBatchTask(issueBundleRequest.getCurrentBatchTask()+1);
            logger.info("all tasks data {}",issueBundleRequest.getAllTasksDetails());
            logger.info("time taken {}",System.currentTimeMillis()-issueBundleRequest.getTimeStamp());
            logger.info("lag in receiving  request {}",issueBundleRequest.getTimeStamp()-(issueBundleRequest.getEventTime()*1000));
            completedTasks.clear();
            completedTasksDetails.clear();
            dynamicTasksForCurrentBatch.clear();
            collector.collect(issueBundleRequest);
        }
    }
}
