package in.aether.bundles.flink.processors;

import in.aether.bundles.flink.models.IssueBundleRequest;
import org.apache.flink.api.java.functions.KeySelector;

public class BundleKeySelector implements KeySelector<IssueBundleRequest,String> {

    @Override
    public String getKey(IssueBundleRequest o) throws Exception {
        return o.getId();
    }
}
