package in.aether.bundles.flink.processors;

import in.aether.bundles.flink.builder.TaskListBuilder;
import in.aether.bundles.flink.models.IssueBundleRequest;
import in.aether.bundles.flink.models.TaskDetails;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

public class BatchPlanner extends ProcessFunction<String, IssueBundleRequest> {

    Logger logger = LoggerFactory.getLogger(BatchPlanner.class);
    ObjectMapper mapper = new ObjectMapper();

    @Override
    public void processElement(String issueBundleRequest, Context context, Collector<IssueBundleRequest> collector) throws Exception {
        logger.info("executing issueBundleRequest {}", issueBundleRequest);
        IssueBundleRequest issueBundleRequest1 = mapper.convertValue(mapper.readTree(issueBundleRequest), IssueBundleRequest.class);
        issueBundleRequest1.setTimeStamp(System.currentTimeMillis());
        issueBundleRequest1.setTasks(getTasks());
        issueBundleRequest1.setTotalTasks(5);
        collector.collect(issueBundleRequest1);
    }

    //test planner, these tasks will be create based on dependency and schaas details
    private List<List<TaskDetails>> getTasks(){
        List<List<TaskDetails>> tasks = new LinkedList<>();
        TaskDetails getAccountProduct = new TaskDetails(1,"pending","GetAccountProduct");
        TaskDetails getPaymentProduct = new TaskDetails(2,"pending","getPaymentProduct");
        TaskDetails getResource = new TaskDetails(3,"pending","getResourceProduct");
        List<TaskDetails> firstBatchNodes = new TaskListBuilder()
                .addTask(getAccountProduct)
                .addTask(getPaymentProduct)
                .addTask(getResource)
                .getStageTasks();
        tasks.add(firstBatchNodes);
        TaskDetails createResource = new TaskDetails(4,"pending","createResource");
        TaskDetails createAccount = new TaskDetails(5,"pending","createAccount");
        List<TaskDetails> secondBatchNodes = new TaskListBuilder()
                .addTask(createAccount)
                .addTask(createResource).getStageTasks();
        tasks.add(secondBatchNodes);
        return tasks;
    }
}
