package in.aether.bundles.flink.processors;

import in.aether.bundles.flink.models.IssueBundleRequest;
import in.aether.bundles.flink.models.TaskDetails;
import in.aether.bundles.flink.models.TaskExecutorDetails;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class TriggerDependentTasks extends ProcessFunction<IssueBundleRequest,TaskExecutorDetails> {

    Logger logger = LoggerFactory.getLogger(TriggerDependentTasks.class);



    @Override
    public void processElement(IssueBundleRequest issueBundleRequest, Context context, Collector<TaskExecutorDetails> collector) throws Exception {
        issueBundleRequest.setTotalTasksForCurrentExecutors(issueBundleRequest.getTotalDependentTasks().size());
        for(Integer tasks: issueBundleRequest.getTotalDependentTasks()){
            TaskDetails taskDetails = issueBundleRequest.getDependentTasksDetails().get(tasks);
            TaskExecutorDetails taskExecutorDetails = new TaskExecutorDetails(issueBundleRequest,taskDetails);
            collector.collect(taskExecutorDetails);
        }
    }
}
