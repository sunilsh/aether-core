package in.aether.bundles.flink.builder;


import in.aether.bundles.flink.models.TaskDetails;

import java.util.LinkedList;
import java.util.List;

public class TaskListBuilder {
    List<TaskDetails> stageTasks = new LinkedList<>();

    public TaskListBuilder addTask(TaskDetails taskDetails){
        this.stageTasks.add(taskDetails);
        return this;
    }

    public List<TaskDetails> getStageTasks() {
        return stageTasks;
    }
}
