package in.aether.bundles.flink.filters;

import in.aether.bundles.flink.models.IssueBundleRequest;
import org.apache.flink.api.common.functions.FilterFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TaskFilter implements FilterFunction<IssueBundleRequest> {

    Logger logger = LoggerFactory.getLogger(TaskFilter.class);

    @Override
    public boolean filter(IssueBundleRequest issueBundleRequest) throws Exception {
        logger.info("inside TaskFilter for issueBundleRequest {}",issueBundleRequest);
        List<Integer> sequentialTasks = new ArrayList<>();
        HashMap<Integer,Integer> visitedTasks = new HashMap<>();
        if(issueBundleRequest.getTotalDependentTasks().size()>0 && issueBundleRequest.getCompletedTasks().size()==issueBundleRequest.getTotalDependentTasks().size()){
            return false;
        }
        for(Integer currentTask: issueBundleRequest.getTotalDependentTasks()) {
            if(checkCycleAndGetTasks(issueBundleRequest.getGraph(), visitedTasks, currentTask, sequentialTasks)){
                logger.info("cycle found");
                return  false;
            }
        }
        logger.info("total tasks in order {}", sequentialTasks);
        issueBundleRequest.setTotalDependentTasks(sequentialTasks);
        return true;
    }
    public boolean checkCycleAndGetTasks(HashMap<Integer, List<Integer>> adjacency, HashMap<Integer,Integer> visitedTasks, Integer currentTask, List<Integer> sequentialTasks){
        //2 means node is already processed and there is no cycle
        if(visitedTasks.containsKey(currentTask) && visitedTasks.get(currentTask)==2){
            return false;
        }
        if(visitedTasks.containsKey(currentTask) && visitedTasks.get(currentTask)==1){
            return true;
        }
        visitedTasks.put(currentTask,1);
        for(Integer course:adjacency.getOrDefault(currentTask,new ArrayList<>())){
            //System.out.println("dfs for course "+ course);
            if(checkCycleAndGetTasks(adjacency,visitedTasks,course,sequentialTasks)){
                return true;
            }
        }
        sequentialTasks.add(currentTask);
        //setting 2 to check it does not contains a cycle
        visitedTasks.put(currentTask,2);
        return false;
    }
}
